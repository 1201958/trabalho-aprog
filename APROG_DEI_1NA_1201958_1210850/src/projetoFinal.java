/*
Trabalho Prático Realizado por:
1201958 - Marco Lourenço
1210850 - Duarte Fonseca
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class projetoFinal {
    //################### Variaveis ###########################################################################

    static final String FILE_NAME = "teste.txt"; //Guarda em variavel a Localização ficheiro

    //################### Métodos #############################################################################
    public static int[][] lerFicheiros(String file) throws FileNotFoundException {  //Método para ler Ficheiro

        Scanner ler = new Scanner(new File(file));

        String textoDescritivo = ler.nextLine();  // Lê descrição do array
        int sizeL = ler.nextInt();
        int sizeC = ler.nextInt();  // Lê tamanho do array
        //adicionar if para [0]
        ler.nextLine();
        int[][] arr = new int[sizeL][sizeC];

        while (ler.hasNextLine()) {
            for (int i = 0; i < arr.length; i++) {
                String[] line = ler.nextLine().trim().split(" ");
                for (int j = 0; j < line.length; j++) {
                    arr[i][j] = Integer.parseInt(line[j]);
                }
            }
        }

        ler.close();  // Fecha o Ficheiro

        return arr; // Retorna um Array
    }
    public static void printFunc(int[][] dados) {            // Método Impressão

        for (int i = 0; i < dados.length; i++) {
            for (int j = 0; j < dados[i].length; j++)
                System.out.printf("% d ", dados[i][j]);

            System.out.println();
        }
    }
    public static int[][] novoArr2 (int[][] arrEntrada) {          // Método para o novo Array Alterado
        int sizeL = arrEntrada.length;
        int sizeC = arrEntrada[0].length;
        int[][]arrSaida=new int[sizeL][sizeC]; //Criar novo array com dimisoes iguais ao anterior
        for (int i = 0; i <sizeL ; i++) {
            for (int j = 0; j < sizeC; j++){
                arrSaida[i][j] = arrEntrada[i][j];}
        }//copiar valores do array
        return arrSaida;
    }
    public static int[][] novoArr (int[][] arrEntrada) {          // Método para o novo Array Alterado
        int sizeL = arrEntrada.length;
        int sizeC = arrEntrada[0].length;
        int[][] novoArr=new int[sizeL][sizeC]; //Criar novo array com dimisoes iguais ao anterior
        for (int i = 0; i <sizeL ; i++) {
            for (int j = 0; j < sizeC; j++){
                novoArr[i][j] = arrEntrada[i][j] + 1;}
        }//incrementar o valor do array de entrada +1
        return novoArr;
    }

    public static int[] cuboFunc (int[][] arrAlterado){ // i)
        int[]arrCoordernadasTerraMovida= new int [3]; // array para retorno
        int dimensaoCubo=3;
        int linha; // linha do array
        int coluna; //coluna do array
        int linhaCubo; //linha do cubo
        int colunaCubo; //coluna do cubo
        int sizeL = arrAlterado.length; //Numero linhas do arrayz
        int sizeC = arrAlterado[0].length; //Numero colunas do array
        int quantidadeTerraMin=0; //valor min de terra necessaria (server para comparação)
        int quantidadeTerra; // numero de terra a ser movida
        for (linha = 0; linha < (sizeL-2) ; linha++) {
            for (coluna = 0; coluna < (sizeC-2); coluna++) {
                quantidadeTerra = 0;
                for (linhaCubo = linha; linhaCubo <= linha + 2; linhaCubo++) {
                    //Loop de Cubos
                    for (colunaCubo = coluna; colunaCubo <= coluna + 2; colunaCubo++) {

                        if (arrAlterado[linhaCubo][colunaCubo] > dimensaoCubo*(-1)) {
                            quantidadeTerra = quantidadeTerra + (arrAlterado[linhaCubo][colunaCubo]+dimensaoCubo); // Somar quantidade de terra
                        } else {
                            quantidadeTerra = quantidadeTerra + ((arrAlterado[linhaCubo][colunaCubo]+dimensaoCubo)*(-1)); // Somar quantidade de terra e multiplicar por -1 para transformar em quantidade positiva
                        }
                    }
                }
                if (linha ==0 && coluna==0){ // atribuir o valor minimo de terra caso para comparação futura e garantir casos seja o menor valor,em caso de múltiplas opções com o mesmo valor apresenta o mais Norte e a Oeste.
                    quantidadeTerraMin=quantidadeTerra; //atribui o valor de terra ao valor minimo
                    arrCoordernadasTerraMovida[0]=quantidadeTerraMin; // Guarda o valor no array de resultados (terra a mover)
                    arrCoordernadasTerraMovida[1]=linha; // Guarda o valor no array de resultados (Coordenadas linha)
                    arrCoordernadasTerraMovida[2]=coluna; // Guarda o valor no array de resultados (Coordenadas linha coluna)
                }
                if (quantidadeTerra<quantidadeTerraMin){
                    arrCoordernadasTerraMovida[0]=quantidadeTerra; // Guarda o valor no array de resultados (terra a mover)
                    arrCoordernadasTerraMovida[1]=linha; // Guarda o valor no array de resultados (Coordenadas linha)
                    arrCoordernadasTerraMovida[2]=coluna; // Guarda o valor no array de resultados (Coordenadas linha coluna)
                }

            }
        }

        return arrCoordernadasTerraMovida;
    }

    public static double[] ContadorNumerosNegativosPositivos(int[][] arr) {          // Método para calcular area inundada
        int sizeL = arr.length;
        int sizeC = arr[0].length;
        double[] ContadorNumerosPosNeg= new double[4];
        for (int i = 0; i <sizeL ; i++)
            for (int j = 0; j < sizeC; j++)
                if (arr[i][j]<0) {
                    ContadorNumerosPosNeg[0]++; //Contador de valores Negativos
                    ContadorNumerosPosNeg[2]+=arr[i][j];
                 }else
                {
                    ContadorNumerosPosNeg[1]++; //Contador de valores Positvos
                    ContadorNumerosPosNeg[3]+=arr[i][j];
                }

        //Posição 0 do array retorna Numero de Negativos --> ContadorNumerosPosNeg[0]
        //Posição 1 do array retorna Numero de Positivos --> ContadorNumerosPosNeg[1]
        //Posição 2 do array retorna Incremento de Negativos --> ContadorNumerosPosNeg[2]
        //Posição 3 do array retorna Incremento de Positivos --> ContadorNumerosPosNeg[3]
        return ContadorNumerosPosNeg;
    }


    public static int variacaoInundada(int[][] arr1 , int [][] arr2){
        int sizeL = arr1.length;
        int sizeC = arr1[0].length;
        int contadorNegArr1=0;
        int contadorNegArr2=0;
        for (int i = 0; i <sizeL ; i++)
            for (int j = 0; j < sizeC; j++)
                if (arr1[i][j]<0) {
                    contadorNegArr1++;  //Contador de valores Negativos antigo
                }
        for (int i = 0; i <sizeL ; i++)
            for (int j = 0; j < sizeC; j++)
                if (arr2[i][j]<0) {
                    contadorNegArr2++;  //Contador de valores Negativos array novo
                }

        int difArray=contadorNegArr2-contadorNegArr1; //Diferença entre array, calcula a diferença de arena inundada
     return difArray;
    }


    public static int[]  valorMaxMinFunc(int[][] arr){  //Metodo não necessario para o exercio (apenas para teste)
        int[]valorMaxMin = new int[2];
        int sizeL = arr.length;
        int sizeC = arr[0].length;
        valorMaxMin[1]=-99999;
        valorMaxMin[0]=99999;
        for (int i = 0; i <sizeL ; i++) {
            for (int j = 0; j < sizeC; j++) {
                    if (arr[i][j] < valorMaxMin[0]) {

                        valorMaxMin[0]=arr[i][j];

                    }else if (arr[i][j] > valorMaxMin[1]){
                        valorMaxMin[1] = arr[i][j];
                    }
            }
        }
        //valorMaxMin[0] valor Minimo do array
        //valorMaxMin[1] valor Maximo do array
        return valorMaxMin;
    }
    private static double volumeArrNegativos(int[][] arrAlterado) {
        double []volumeArr=ContadorNumerosNegativosPositivos(arrAlterado);
        double formulaVolume = volumeArr[0]*(volumeArr[2]/volumeArr[0]);

        return formulaVolume;
    }

    private static boolean positivosFunc (int[][]arr){
        boolean arrayContemPositivos=false;
        int sizeL = arr.length;
        int sizeC = arr[0].length;
        for (int i = 0; i <sizeL ; i++) {
            for (int j = 0; j < sizeC; j++) {
                if (arr[i][j]>=0){
                    arrayContemPositivos=true;
                }
            }
        }
        return arrayContemPositivos;
    }
    private static void subidaAgua(int[][]arr) {
        int sizeL = arr.length;
        int sizeC = arr[0].length;
        int[]arrSubidaInundada=new int[2];
        //arrSubidaInundada[0] Subida Agua
        //arrSubidaInundada[1] Arena inundada m2
        int[][]arrSubida;
        arrSubida=novoArr2(arr);
       // arrSubidaInundada[0]=1;

        do  {
            arrSubidaInundada[1]=0;
            for (int i = 0; i < sizeL; i++) {
                for (int j = 0; j < sizeC; j++) {
                    if (arrSubida[i][j]>=0) {
                        arrSubida[i][j] -= 1;
                        if(arrSubida[i][j]<0){
                        arrSubidaInundada[1]++;


                        }
                    }

                }

            }
            arrSubidaInundada[0]++;
            System.out.printf("%18d | %18d\n",arrSubidaInundada[0],arrSubidaInundada[1]);
        }while(positivosFunc(arrSubida));



    }

    private static int inundarFunc(int valorMax) {
        int metrosAgua=0;
        while (valorMax>=0){
            valorMax=valorMax-1;
            metrosAgua++;

        }

        return metrosAgua;
    }

    private static int colunaPostivos(int[][] arr) { //in progress
        int linha=0;
        int coluna=0;
        int[][] newArr;
        int returnColuna=-1;
        newArr=arr;
        int colunaSeca;
        boolean caminhoSeco=false;
        int sizeL = arr.length;
        int sizeC = arr[0].length;
        for (coluna = arr[0].length - 1; coluna >= 0; coluna--)
         {
             for (linha = arr.length-1 ; linha>=0  ; linha--) {
                    newArr[linha][coluna] = arr[linha][coluna];
                   // teste System.out.printf("% d ", newArr[linha][coluna]);
                    if (newArr[linha][coluna] >= 0) {
                        caminhoSeco = true;
                    } else {caminhoSeco = false;}

                }
             if (caminhoSeco==true){
                 returnColuna=coluna;
                 //System.out.printf("caminho seco na vertical na coluna (%d)",coluna);
                 coluna=0;
             }
             //System.out.println();
            }

            //if (linha==arr.length-1 && caminhoSeco==true){
              //  System.out.printf("caminho seco na vertical na coluna (%d)",coluna);
         //   }

        return returnColuna;
        }



    //################## Main ################################################################################

    public static void main(String[] args )throws FileNotFoundException {

        //Ex A)
        int[][] ficheiro = lerFicheiros(FILE_NAME);  // Leitura do ficheiro, criação de array

        //Ex B)
        System.out.println("b)"); //Print do array
        printFunc(ficheiro);

        //Ex C)
        System.out.println("c)"); //Update da array e Print da nova array
        int[][] arrAlterado=novoArr(ficheiro);
        printFunc(arrAlterado);

        //Ex D)
        System.out.println("d)"); //Calcular a percentegem de area inundada
        double[] percNeg= ContadorNumerosNegativosPositivos(arrAlterado);
        double resultadoPercNeg =(percNeg[0]/(percNeg[0]+percNeg[1])*100);
        System.out.printf("area submersa: %.2f%%\n",resultadoPercNeg);

        //Ex E)
        System.out.println("e)"); //Calcular a variação da área inundada
        variacaoInundada(ficheiro,arrAlterado);
        System.out.printf("variacao da area inundada: %d m2\n",variacaoInundada(ficheiro,arrAlterado));

        //Ex F)
        System.out.println("f)"); //Visualizar o volume de água existente no terreno;
        double volumeAgua=volumeArrNegativos(arrAlterado)*(-1);
        System.out.printf("volume de agua: %.0f m2\n",volumeAgua);

        //Ex G)
        System.out.println("g)"); //Visualizar quantos metros terá de subir a água para inundar o terreno
        int[] minMax=valorMaxMinFunc(arrAlterado);
        int valorMax=minMax[1];
        inundarFunc(valorMax);
        System.out.printf("para inundacao total, subir :%d m \n",inundarFunc(valorMax));

        //Ex H)
        System.out.println("h)"); //Visualizar os incrementos em m2 de área inundada
        System.out.printf("subida da agua (m) | area inundada (m2)\n" + "------------------ | ------------------\n");
        subidaAgua(arrAlterado);


        //Ex I)
        System.out.println("i)"); //Calcular coordenadas e terraMovida
        int[] arrCubo=cuboFunc(arrAlterado);
        System.out.printf("coordenadas do cubo: (%d,%d), terra a mobilizar: %d m2 \n",arrCubo[1],arrCubo[2],arrCubo[0]);

        //Ex J)
        System.out.println("j)"); //Procurar um caminho seco
        int numoroColuna=colunaPostivos(arrAlterado);
        if (numoroColuna==-1){
            System.out.println("não há caminho seco na vertical");
        }else {
            System.out.printf("caminho seco na vertical na coluna (%d)\n", numoroColuna);
        }
    }



}
